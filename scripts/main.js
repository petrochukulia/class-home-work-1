document.write("<header></header>");
document.write("<nav></nav>");
document.write("<section></section>");
document.write("<footer></footer>");

var userName = prompt("Вкажіть ваше ім'я", "тут");
var surName = prompt("Вкажіть ваше прізвище");
var age = prompt("Вкажіть ваш вік", "25");

document.write("Ваше ім'я: " + userName + "<br>");
document.write("Ваше прізвище: " + surName + "<br>");
document.write("Ваш вік: " + surName + "<br>");

var number1 = prompt("Введіт перше число");
var number2 = prompt("Введіт друге число");
var number3 = prompt("Введіт третє число");

var normalNumber1 = parseFloat(number1);
var normalNumber2 = parseFloat(number2);
var normalNumber3 = parseFloat(number3);

var average = (normalNumber1 + normalNumber2 + normalNumber3)/3;
document.write("Середнє арифметичне чисел " + number1 +", " + number2 + " і " + number3 + " = " + average + "<br>");

var x = 6;
var y = 14;
var z = 4;
x += y - x++ * z // постфіксне збільшення не враховується тут,  х*z = 6*4= 24; y-24 = 14-24 = -10; х = х+(-10)= 6-10 = -4.
document.write("x += y - x++ * z = " + x + "<br>");

var x = 6;
var y = 14;
var z = 4;
z = --x - y * 5 // x зменшується = 5; У*5 = 70; 5-70 = -65.
document.write("z = --x - y * 5 = " + z + "<br>");


var x = 6;
var y = 14;
var z = 4;
y /= x + 5 % z; // 5%4=1; x+1 = 6 +1= 7; 14/7 = 2.
document.write("y /= x + 5 % z = " + y + "<br>");


var x = 6;
var y = 14;
var z = 4;
z - x++ + y * 5; // постфіксне збільшення не враховується, у*5 = 14*5= 70; z-x=4-6=-2, -2+70=68.
document.write("z - x++ + y * 5 = " + (z - x++ + y * 5) + "<br>"); // виводиться 67 так як спрацьовує х++


var x = 6;
var y = 14;
var z = 4;
x = y - x++ * z; // постфіксне збільшення не враховується, x*z=6*4=24; y-24=14-24=-10.
document.write("x = y - x++ * z; = " + x + "<br>");
